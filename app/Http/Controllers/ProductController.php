<?php

namespace ProductsModule\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        $products = \ProductsModule\Product::All();
        return view('index', compact('products'));
    }

    public function listing(){
        $products = \ProductsModule\Product::All();
        return response()->json(
            $products->toArray()
        );
    }

    public function create(){
        return view('create');
    }

    public function destroy($id){
        $product = \ProductsModule\Product::find($id);
        $product->delete();
        return response()->json(
            $product->toArray()
        );
    }

    public function store(Request $request){
        if($request->ajax()){
            \ProductsModule\Product::create($request -> all());
            return response()->json([
                "mensaje" => "Creado con exito"
            ]);
        }
    }
}
