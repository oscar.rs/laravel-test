<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductController@index');
Route::get('create', 'ProductController@create');
Route::post('/product', 'ProductController@store');
Route::get('/products', 'ProductController@listing');
Route::delete('/product/{id}', 'ProductController@destroy');