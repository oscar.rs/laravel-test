var products = [];

$( document ).ready(function() {
    index();
    create();
});

var routes = {
    index: "http://localhost:8000/products",
    show: "http://localhost:8000/product/",
}



function index(){
    $.get(routes.index, function(response){
        $('#productsData').empty();
        products = response;
        $(response).each(function(key, value){
            $('#productsData').append(
                '<div class="col-xs-6 col-md-3"> \
                    <div class="box-product"> \
                        <div class="box-image" data-toggle="modal" data-target="#show-modal"  onclick="show(this.id)" id="' + value.id + '"> \
                        </div> \
                        <div class="box-information"> \
                            <h3> ' + value.name + '</h3>\
                            <div class="description"> \
                                ' + value.description + '\
                            </div>\
                            <div class="button-group"> \
                                <button class="btn btn-default" onclick="show(this.id)" id="' + value.id + '" data-toggle="modal" data-target="#show-modal">Ver mas</button>\
                                <div class="btn btn-danger" onclick="remove(this.id)" id="' + value.id + '">Eliminar</div>\
                            </div>\
                        </div>\
                    </div> \
                </div>'
            );
        })
    })
}


function validate(object){
    var error = Object.keys(object).find(function(key) {
        return !object[key] 
    });
    return !error
}

function create(){
    $('#createProduct').click(function(){
       var token = $('#token').val();
       var data = {
           name: $('#name').val(),
           description: $('#description').val(),
           price: $('#price').val(),
           stock: $('#quantity').val()
       }
        if(validate(data)){
            $.ajax({
                url: routes.show,
                headers: {'X-CSRF-TOKEN' : token},
                type: 'POST',
                dataType: 'json',
                data: data,
            }).done(function(){
                index();
                setValues();
                $('#create-modal').modal('toggle');
            })
        }else{
            alert('Llene todos los campos');
        }
    })
}

function setValues(){
    $('#name').val(''),
    $('#description').val(''),
    $('#price').val(''),
    $('#quantity').val('')
}

function remove(id){
    var token = $('#token').val();
        $.ajax({
            url: routes.show + id,
            headers: {'X-CSRF-TOKEN' : token},
            type: 'DELETE',
            dataType: 'json',
        }).done(function(){
            index();
        })
}

function show(id){
    var selectProduct = products.find(function(product){
        return product.id === parseInt(id);
    })
    $('#title-product').text(selectProduct.name)
    $('#show-description').text(selectProduct.description)
    $('#show-price').text(selectProduct.price+ '$')
    $('#show-stock').text(selectProduct.stock)
}


function limitText(field, maxChar){
    var ref = $(field),
        val = ref.val();
    if ( val.length >= maxChar ){
        ref.val(function() {
                return val.substr(0, maxChar);       
        });
    }
}

function onlyNumbers(event) {
    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 
        || event.keyCode == 27 || event.keyCode == 13 
        || (event.keyCode == 65 && event.ctrlKey === true) 
        || (event.keyCode >= 35 && event.keyCode <= 39)){
            return;
    }else {
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault(); 
        }   
    }
};
