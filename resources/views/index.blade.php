<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<script
  src="http://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous">
</script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="js/code.js"></script>
<link href="https://fonts.googleapis.com/css?family=Oleo+Script:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Teko:400,700" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<body>


<div class="jumbotron jumbotron-fluid main animated fadeIn fast">
  <div class="container">
            <h1 class="section-header"><span class="content-header wow fadeIn " data-wow-delay="0.2s" data-wow-duration="2s"> Bar Foo</span>  Store</h1>
    <p class="lead">Obten tu stock gratis !</p>
  </div>
  <div class="create-product" data-toggle="modal" data-target="#create-modal">  
    Crear producto
  </div>
</div>


<div class="row" id="productsData">
</div>


<!-- Create Product  Modal-->
<div id="create-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <section id="contact">
          <div class="section-content">
            <h1 class="section-header">Agregar <span class="content-header wow fadeIn " data-wow-delay="0.2s" data-wow-duration="2s"> Producto</span></h1>
            <h3>Ingresa los siguientes campos para agregar un producto</h3>
          </div>
          <div class="contact-section">
            <form>
              <div class="row">
                  <input type="hidden"  name="_token" value="{{ csrf_token() }}" id="token">
                  <div class="col-md-12 col-xs-12">
                      <div class="form-group">
                          <label for="name">Nombre</label>
                          <input type="text" class="form-control" onkeyup="limitText(this,35)" id="name" placeholder=" Nombre">
                      </div>
                      <div class="form-group">
                          <label for="description">Descripcion</label>
                          <input type="text" class="form-control" onkeyup="limitText(this,50)" id="description"  placeholder=" Descripcion">
                      </div>	
                      <div class="form-group">
                          <label for="price">Precio</label>
                          <input type="tel" class="form-control" id="price" onkeydown="onlyNumbers(event)" onkeyup="limitText(this,4)" placeholder=" Precio">
                      </div>
                      <div class="form-group">
                          <label for="quantity">Cantidad</label>
                          <input type="number" onkeydown="onlyNumbers(event)" onkeyup="limitText(this,3)" class="form-control" id="quantity" placeholder=" Cantidad">
                      </div>
                  </div>
              </div>
            </form>
        </section>
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" id="createProduct">Crear</button>
      </div>
    </div>

  </div>
</div>


<!-- Show Product  Modal-->
<div id="show-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <section id="contact">
          <div class="section-content">
            <div class="row">
              <div class="col-xs-12 col-md-6 col-sm-6">  
                <div class="box-image show-product"></div>
              </div>
              <div class="col-xs-12 col-md-6 col-sm-6">
                  <h1 class="section-header"><span id="title-product" class="content-header wow fadeIn " data-wow-delay="0.2s" data-wow-duration="2s"></span></h1>
                  <h3 id="show-description"></h3>
                  <div>Precio <span id="show-price"></span> </div>
                  <div>Stock <span id="show-stock"></span> </div>
              </div>
            </div>
          </div>
          <!-- <div class="contact-section">
            <form>
              <div class="row">
                  <input type="hidden"  name="_token" value="{{ csrf_token() }}" id="token">
                  <div class="col-md-12 col-xs-12">
                      <div class="form-group">
                          <label for="exampleInputUsername">Nombre</label>
                          <input type="text" class="form-control" id="name" placeholder=" Nombre">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail">Descripcion</label>
                          <input type="email" class="form-control" id="description" placeholder=" Descripcion">
                      </div>	
                      <div class="form-group">
                          <label for="telephone">Precio</label>
                          <input type="tel" class="form-control" id="price" placeholder=" Precio">
                      </div>
                      <div class="form-group">
                          <label for="telephone">Cantidad</label>
                          <input type="tel" class="form-control" id="quantity" placeholder=" Cantidad">
                      </div>
                  </div>
              </div>
            </form> -->
        </section>
      </div>
    </div>

  </div>
</div>
    
</body>
</html>

